const app = require('express')();
const bodyParser = require ('body-parser');
const cors = require('cors');
const nodemailer = require('nodemailer');

app.use(cors());
app.use(bodyParser.urlencoded({ extended: true }));


app.post('/form-submission', function (req, res) {
  sendEmail(req.body, res)
});

app.listen(3000, function () {
  console.log('POST to /form-submission');
});


function sendEmail(data, res) {

  var transporter = nodemailer.createTransport('smtps://server%40motivated.tech:qwerty12345@smtp.zoho.com');

  var mailOptions = {
    from: '"Motivated.tech Form" <server@motivated.tech>',
    to: 'benj.calderon@gmail.com, benj@motivated.tech',
    subject: 'Form Submission ' + Date(),
    text: `Name: ${data.name}\nEmail: ${data.email}\nMessage: ${data.message}`,
  };

  transporter.sendMail(mailOptions, function(error, info){
    if(error){
      return console.log(error);
    }
    console.log('Message sent: ' + info.response);
    res.status(200).send('done\n')
  });



}
